from django import forms

class SubscribeForm(forms.Form):
    """
    Simple subscribe form.
    """
    email = forms.EmailField()