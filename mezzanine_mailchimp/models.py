# coding=utf-8

import mailchimp

from django.dispatch.dispatcher import receiver
from mezzanine.conf import settings
from mezzanine.forms.signals import form_valid


@receiver(form_valid)
def suscribe_mailchimp(sender, **kwargs):
    if settings.MAILCHIMP_FORM_SIGNAL:
        entry = kwargs.get('entry')
    
        if entry is not None:
    
            # Buscamos todos los campos del form que tengan como campo el email
            fields_email = entry.form.fields.filter(field_type=3).values_list('id', flat=True)
    
            if fields_email:
                # Buscamos todos los resultados para los campos con el email.
                results_emails = entry.fields.filter(field_id__in=fields_email).values_list('value', flat=True)
    
                if results_emails:
                    list_id = settings.MAILCHIMP_LIST_ID.decode("utf-8")
                    api_key = settings.MAILCHIMP_API_KEY.decode("utf-8")
    
                    # Convertimos los emails en diccionarios para el batch
                    lists_emails = [{'email': {'email': email}} for email in results_emails]
                    try:
                        m = mailchimp.Mailchimp(api_key)
    
                        # Hacemos la peticion para suscribir los emails
                        print m.lists.batch_subscribe(list_id, lists_emails)
    
                    except mailchimp.ListAlreadySubscribedError:
                        print 'los correos ya estan asociados'
    
                    except mailchimp.Error, e:
                        print 'error'
                        print e
