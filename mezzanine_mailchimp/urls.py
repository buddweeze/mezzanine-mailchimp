from django.conf.urls import patterns, url
from django.contrib import admin

urlpatterns = patterns("",
    url("^subscribe/$", "mezzanine_mailchimp.views.subscribe", name="mailchimp-subscribe"),
)
