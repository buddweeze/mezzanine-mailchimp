from django.shortcuts import render
from django.http import HttpResponse
from mezzanine.conf import settings
from mezzanine_mailchimp.forms import SubscribeForm
import mailchimp
import simplejson

def subscribe(request):
    if request.method == 'POST':
        form = SubscribeForm(request.POST)
        if form.is_valid():
            try:
                m = mailchimp.Mailchimp(settings.MAILCHIMP_API_KEY.decode("utf-8"))
                m.lists.subscribe(settings.MAILCHIMP_LIST_ID.decode("utf-8"), {"email": form.cleaned_data['email'],})
                message = "Thanks for Subscribing"
            except mailchimp.ListAlreadySubscribedError:
                message = "Already Subscribed"
            except mailchimp.Error, e:
                print e
                message = "An Error Occured"
            if request.is_ajax():
                return HttpResponse(simplejson.dumps({'message': message}), content_type='application/json')
            else:
                print "NOT AJAX"
    else:
        form = SubscribeForm()
        message = None
    return render(request, 'mailchimp/subscribe.html', {
        'form': form,
        'message': message,
    })