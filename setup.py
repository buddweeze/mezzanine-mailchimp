from setuptools import setup, find_packages

import os
if os.path.exists("README.txt"):
    readme = open("README.txt")
else:
    print "Warning: using markdown README"
    readme = open("README.md")

setup(
    name = "mezzanine-mailchimp",
    version = "0.1",
    description = "Email subscribe forms of mezzanine to a MailChimp list",
    long_description = readme.read(),
    author = "James Pells",
    author_email = "jimmy@jamespells.com",
    license = "BSD",
    url = "https://bitbucket.org/lvelezsantos/mezzanine-mailchimp/",
    setup_requires=("setuptools"),
    install_requires=(
        "setuptools",
        "mezzanine >= 1.4.16",
        "mailchimp == 2.0.7",
        "simplejson",
    ),
    packages = find_packages(),
    package_data = {
    },
    classifiers=[
        "Development Status :: 1 - Beta",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Framework :: Django",
    ],
)
